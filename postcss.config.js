module.exports = {
  plugins: {
    'precss': {},
    'autoprefixer': {
      grid: true // add browser prefixes for CSS grid
    },
    //'cssnano': {}
  }
}

/**
 * AUTOPREFIXER OPTIONS
 */

// env (string): environment for Browserslist.
// cascade (boolean): should Autoprefixer use Visual Cascade, if CSS is uncompressed. Default: true
// add (boolean): should Autoprefixer add prefixes. Default is true.
// remove (boolean): should Autoprefixer [remove outdated] prefixes. Default is true.
// supports (boolean): should Autoprefixer add prefixes for @supports parameters. Default is true.
// flexbox (boolean|string): should Autoprefixer add prefixes for flexbox properties. With "no-2009" value Autoprefixer will add prefixes only for final and IE versions of specification. Default is true.
// grid (boolean): should Autoprefixer add IE prefixes for Grid Layout properties. Default is false.
// stats (object): custom usage statistics for > 10% in my stats browsers query.
// browsers (array): list of queries for target browsers. Try to not use it.  The best practice is to use .browserslistrc config or browserslist key in package.json to share target browsers with Babel, ESLint and Stylelint. See Browserslist docs for available queries and default value.
// ignoreUnknownVersions (boolean): do not raise error on unknown browser version in Browserslist config or browsers option. Default is false.