// Main SASS entry point
import './sass/styles.scss';

// Bootstrap JS
import 'bootstrap';

(function($) {

    $('[data-toggle="popover"]').popover()   
      
})( jQuery );
