const path = require('path');
const etp = require('extract-text-webpack-plugin');

module.exports = {

    mode: 'development',
    entry: './src/index.js',

    output: {
        path: path.resolve(__dirname, 'dist/'),
        filename: 'bundle.js'
    },

    // Adding jQuery as external library
    externals: {
        jquery: 'jQuery'
    },

    module: {

        rules: [
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: etp.extract({
                    fallback: 'style-loader', // CSS inject to page
                    use: [
                        'css-loader', // CSS into commonJS modules
                        'postcss-loader', // postCSS actions
                        'sass-loader' // SASS to CSS
                    ]
                })
            },

            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: [require('@babel/plugin-proposal-object-rest-spread')]
                    }
                }
            },

            {
                test: /\.jpe?g$|\.gif$|\.png$|\.svg$/i,
                loader: "file-loader?name=/img/[name].[ext]"
            }
        ]
    },

    plugins: [
        new etp(
            {
                filename: 'styles.css'
            }
        )
    ]

}
